Manual
--

* Clone Repo
* Build 

  ``` sudo docker build -t ekiti-app -f ekiti.Dockerfile . ```
* Define host ip env 
  
  ```HOSTIP=`ip -4 addr show scope global dev eth0 | grep inet | awk '{print \$2}' | cut -d / -f 1` ```
* Run 

  ```sudo nohup docker run --add-host=poladtechvm:${HOSTIP} --name ekiti --rm -p 8080:80 -e LOG_STDOUT=true -e LOG_STDERR=true -e LOG_LEVEL=debug -v /var/www/html/ekiti:/var/www/html ekiti-app &>/dev/null & ```
* bash in 
  
  ```sudo docker exec -i -t ekiti bash```
